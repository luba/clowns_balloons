
library(readxl)
library(writexl)

sacc_data <- read_excel(file.choose())
sacc_data <- subset(sacc_data, `CURRENT_SAC_CONTAINS_BLINK` == 'false')

unqSessions <- unique(sacc_data$RECORDING_SESSION_LABEL)
unqLevels <- unique(sacc_data$level)
unqBlocks <- unique(sacc_data$block)

results <- c()

for (sessions in unqSessions) {
  
  currSess <- c()
  
  for (lev in unqLevels) {
    
    for (bl in unqBlocks) {
      
      currData <- subset(sacc_data, 
        RECORDING_SESSION_LABEL == sessions & level == lev & 
        block == bl)
      
      corr <- subset(currData, `if correct response` == 1 & answer != "false")
      currSess <- append(currSess, mean(corr$CURRENT_SAC_START_TIME))
      currSess <- append(currSess, mean(corr$CURRENT_SAC_AMPLITUDE))
      currSess <- append(currSess, mean(corr$CURRENT_SAC_PEAK_VELOCITY))
      
      incorr <- subset(currData, `if correct response` == 0 & answer != "false")
      currSess <- append(currSess, mean(incorr$CURRENT_SAC_START_TIME))
      currSess <- append(currSess, mean(incorr$CURRENT_SAC_AMPLITUDE))
      currSess <- append(currSess, mean(incorr$CURRENT_SAC_PEAK_VELOCITY))
      
      falseFB <- subset(currData, answer == "false")
      currSess <- append(currSess, mean(falseFB$CURRENT_SAC_START_TIME))
      currSess <- append(currSess, mean(falseFB$CURRENT_SAC_AMPLITUDE))
      currSess <- append(currSess, mean(falseFB$CURRENT_SAC_PEAK_VELOCITY))
      
    }
  }
  
  results <- rbind(results, currSess)
  
}

df <- data.frame(results, row.names =  unqSessions)
write_xlsx(df, "sacc_test.xlsx")
